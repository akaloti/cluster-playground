package main

import (
	"fmt"
	"os"
    "strconv"
	"time"
)

func main() {
	// Environment variable will be checked every @interval seconds.
	interval := 1

	// See if user wishes to overwrite @interval with command-line arg.
	if len(os.Args) >= 2 {
		var err error
		interval, err = strconv.Atoi(os.Args[1])
		if err != nil {
			fmt.Println("Interval must be integer")
			os.Exit(1)
		}
	}

	// Print value of environment variable every @interval seconds.
	ticker := time.NewTicker(time.Duration(interval) * time.Second)
	numSeconds := 0
	for {
		select {
		case <- ticker.C:
			numSeconds += interval
			fmt.Println("Second", numSeconds, "| value of FOO is:", os.Getenv("FOO"))
		}
	}
}