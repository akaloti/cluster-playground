#!/bin/bash

docker build -t go-counter:latest .
if test $? -eq 0; then
    docker run --rm go-counter:latest
else
    exit 1
fi
