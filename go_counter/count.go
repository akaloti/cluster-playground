package main

import (
	"fmt"
	"time"
)

func main() {
	ticker := time.NewTicker(time.Second)
	numSeconds := 0
	for {
		select {
		case <- ticker.C:
			numSeconds += 1
			fmt.Println("No. seconds:", numSeconds)
		}
	}
}