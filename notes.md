# Notes

## `minikube`

- `minikube status`
- `minikube start --driver=docker`
    - Modifies `~/.kube/config`
    - Can use `virtualbox` or something else as driver too.
    - `hyperv` possible?
- `minikube` configures `kubectl` to connect to it.
- `minikube dashboard`

- Delete cluster: `minikube delete`

- Start with multiple nodes (doesn't work, at least on WSL 2): `minikube start --driver=docker --nodes 3`
- Add node: `minikube node add`
- `minikube node list`
- `minikube node delete <node name>`

## Using `deployment.yaml`

- `kubectl apply -f deployment.yaml`
- `kubectl get pods`
    - `kubectl get pods -o wide`
- `kubectl get deployment`
- `kubectl delete -f deployment.yaml`

## Using `daemonset.yaml`

- `kubectl apply -f daemonset.yaml`
- `kubectl get daemonset`

## Other

- `kubectl logs <pod name>`
    - Get container name from `kubectl get pods`.
- Kill/delete pod: `kubectl delete pod <pod name>`
- `minikube ssh -n minikube-m05`

## Nodes

- `kubectl get nodes`

## Config; `kubectl` connection to cluster

- `kubectl config view`
- `kubectl cluster-info`
- `aws eks list-clusters`

## Taints

- `kubectl taint nodes minikube-m04 key1=value1:NoSchedule`

## Services

- `kubectl get services`

## Docker (check_env.go Image)

- `docker build -f Dockerfile-check_env -t go-check-env .`
- `docker run -it --rm go-check-env`
- `docker tag go-check-env aaronistheman/go-check-env:latest`
- `docker push aaronistheman/go-check-env:latest`

## Deploying/Using k8s Env Var Setup

- `kubectl apply -f check-env-deployment.yaml`
- `kubectl apply -f check-env-cm.yaml`
- `kubectl exec -ti <pod name> -- /bin/sh -c env`
- `kubectl get cm`

## Steps for Using AWS

Most important Udemy: 247, 248

TODO: What exactly is the part/component(s) that cost money?

- **Create cluster**: Go to AWS EKS page and start creating a cluster on the interface there.
    - **Create cluster service role** ("IAM role to allow the Kubernetes control plane to manage AWS resources on your behalf").
        - Create new role on IAM console.
            - Use case "EKS - Cluster" to get predefined role.
        - QUESTION: Is this role applied to your entire AWS account? If so, would you use it for every EKS cluster you make? Or would there be a unique role per EKS cluster?
    - **Configure the network** (the network to which all the nodes will be added) for the cluster.
        - Must be set up so that:
            - To some extent, it's accessible from outside / the WWW.
            - To some extent, only accessible from inside.
        - Could use VPC console, but Udemy used CloudFormation instead.
            - AWS CloudFormation makes it easier to create certain things based on certain templates.
            - Get URL from here: https://docs.aws.amazon.com/eks/latest/userguide/creating-a-vpc.html#create-vpc
                - I used IPv4 since it looked similar to what Udemy used.
                - The URL (which refers to a YAML file) contains a template for the network we want to create.
                - Why two private subnets and two public subnets?
        - Creating the VPC will take some time, but can continue to next step.
    - Finish with the cluster interface; it will take some time.

- **Setup `aws`**:
    - Install `aws`.
    - Do `aws configure`.
    - `aws eks --region us-east-1 update-kubeconfig --name practice-cluster`
        - The region is determined when you create the cluster earlier.
        - Might need to delete `~/.kube/config` first.

- **Add worker nodes.**
    - **Add Node Group**.
        - Need **create IAM role** for the nodes.
            - Add:
                - AmazonEKSWorkerNodePolicy
                - AmazonEKS_CNI_Policy
                - AmazonEC2ContainerRegistryReadOnly
        - The nodes are EC2 instances.
        - Doesn't just create nodes and add them to cluster. Also installs k8s software like kubelet and kube-proxy and wire them up.

- If deployed a load balancer (with a k8s YAML file), would see it in the AWS Load Balancer page.

=== At this point, cluster is all set up ===

## SSH-ing

- https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/putty.html
- https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/TroubleshootingInstancesConnecting.html#TroubleshootingInstancesConnectingPuTTY

## Viewing AWS Resources

- VPC Console
