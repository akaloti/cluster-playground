#!/bin/bash

docker build -t trivial:latest .
if test $? -eq 0; then
    docker run -it trivial:latest
else
    exit 1
fi
