#!/bin/bash

docker build -t basic_go:latest .
if test $? -eq 0; then
    docker run --rm basic_go:latest $@
else
    exit 1
fi
