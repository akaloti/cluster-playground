package main

import (
	"fmt"
	"os"
)

func main() {
	fmt.Println("Hello, World!!!")
	if len(os.Args) < 2 {
		fmt.Println("You must provide at least one command-line argument.")
	} else {
		fmt.Println("The first arg you provided is:", os.Args[1])
	}
}